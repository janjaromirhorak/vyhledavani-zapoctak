try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

import urllib.request

from .helpers import print_now

ROOT_URL = "https://is.cuni.cz"
SEARCH_DIR = "/studium/predmety/"
SEARCH_PARAMS = "index.php?do=search&nazev=&kod=&match=substring&srch_nazev=0&srch_nazev=1&fak=11320&ustav=&sekce=&trida=&klas=&ujmeno=&utyp=3&pvyjazyk=&sem=&pocet=1000&b=Hledej"
PAGINATION = "&stev_page="
FIRST_PAGE_NUMBER = 1

PAGE_DIR = ROOT_URL + SEARCH_DIR

COLUMNS = ["", "Kód", "Název", "Semestr", "Rozsah, examinace", "Katedra", "Fakulta"]
TAB_CLASS = "tab1"


def get_page_url(page=FIRST_PAGE_NUMBER):
    return "{}{}{}{}{}".format(ROOT_URL, SEARCH_DIR, SEARCH_PARAMS, PAGINATION, page)


def get_subject_codes():
    subject_codes = []

    fetch_next_page = True
    page_number = FIRST_PAGE_NUMBER
    while fetch_next_page:
        print_now("\nFetching page number {}...".format(page_number))

        url = get_page_url(page_number)

        print_now("URL: {}...".format(url))

        request = urllib.request.urlopen(url)
        parsed_dom = BeautifulSoup(request.read(), "lxml")

        # fetch the div with main content
        content_div = parsed_dom.body.find("div", attrs={"id": "content"})

        if not content_div:
            print_now("This page does not have a div with id=\"content\"")
            fetch_next_page = False
            continue

        # fetch all tables with the correct class
        tables = content_div.find_all("table", class_=TAB_CLASS)

        if not tables:
            print_now("This page does not have a table with class \"{}\"".format(TAB_CLASS))
            fetch_next_page = False
            continue

        print_now("Found {} table(s) with the \"{}\" class, searching for the right one...".format(len(tables), TAB_CLASS))

        course_table = None

        # iterate through the tables and find the right one
        for table in tables:
            # get the first row of the table
            first_row = table.find("tr")

            if not first_row:
                # this is clearly not the right table
                continue

            cells = first_row.find_all("td")

            if not cells:
                # this is clearly not the right table
                continue

            # extract text from the cells
            cell_texts = [cell.get_text() for cell in cells]

            # if the cells match the column names, this is the table with courses
            if cell_texts == COLUMNS:
                course_table = table
                break

        if not course_table:
            print_now("The table with courses was not found.")
            fetch_next_page = False
            continue

        # get all rows except the first one (that contains the table header)
        course_rows = course_table.find_all("tr")[1:]

        subjects_added = 0

        for courseRow in course_rows:
            # get the second cell (this cell contains the code)
            cell = courseRow.find_all("td")[1]

            if not cell:
                print_now("There is a corrupted row in the course table.")
                continue

            code = cell.get_text()

            if not code:
                print_now("There is a corrupted row in the course table.")
                continue

            subject_codes.append(code)

            subjects_added += 1

        if subjects_added == 0:
            print_now("This page did not contain any subjects, exiting the pagination loop.")
            fetch_next_page = False
            continue
        else:
            print_now("Added {} subjects.".format(subjects_added))

        page_number += 1

    print("Found codes for %d subjects." % len(subject_codes))

    return subject_codes
