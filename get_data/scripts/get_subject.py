try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

import urllib.request

from .helpers import print_now
from .get_tags import get_tags

ROOT_URL = "https://is.cuni.cz"
SUBJECT_URL = "{}/studium/predmety/index.php?do=predmet&kod=".format(ROOT_URL)


def get_text_box_contents(element):
    # the elements contains three children:
    # 0 - a div containing information about the last edit
    # 1 - a <br/> tag
    # 2... - the text
    #
    # ... fetch only the text and return the contents, filter elements without the string item
    # and join them into one string

    return " ".join([item.string for item in element.contents[2:] if item.string])


def parse_text_box(box_id, parent):
    element = parent.find("div", attrs={"id": box_id})
    if element:
        return get_text_box_contents(element)
    else:
        return ""


def get_subject(code):
    url = "{}{}".format(SUBJECT_URL, code)

    print_now("Fetching {}...".format(url))

    request = urllib.request.urlopen(url)
    parsed_dom = BeautifulSoup(request.read(), "lxml")

    titles = parsed_dom.body.find_all("div", class_="form_div_title")

    title_element = None

    if len(titles) == 0:
        print_now("Unable to find the name of the subject.")
        return None
    elif len(titles) > 1:
        print_now("The title is element is not unique.")
        return None
    else:
        # there is exactly one title element
        title_element = titles[0]

    if not title_element:
        print_now("The title element could not be retrieved from the DOM.")
        return None

    # the title is in format "Subject title - CODE123" - strip the code from the end
    title_text = " - ".join(title_element.get_text().split(" - ")[:-1])

    annotation_text = parse_text_box("pamela_A_CZE", parsed_dom.body)
    syllabus_text = parse_text_box("pamela_S_CZE", parsed_dom.body)
    literature_text = parse_text_box("pamela_L_CZE", parsed_dom.body)

    title_tags = get_tags(title_text)
    text_tags = get_tags(" ".join([annotation_text, syllabus_text, literature_text]))

    return {
        "code": code,
        "title": title_text,
        "annotation": annotation_text,
        "syllabus": syllabus_text,
        "literature": literature_text,
        "title_tags": title_tags,
        "text_tags": text_tags
    }
