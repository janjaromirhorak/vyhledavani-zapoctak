import sys

from ufal.morphodita import *
from urllib.parse import urljoin

from .helpers import print_now

import re

TAGGER_DIR = "../czech-morfflex/"
TAGGER_FILE = "czech-morfflex-pdt-161115.tagger"
TAGGER = urljoin(TAGGER_DIR, TAGGER_FILE)

# first character in the tag determines the part of speech
# more about tag structure: http://ufal.mff.cuni.cz/pdt2.0/doc/manuals/en/m-layer/html/ch02s02s01.html
'''
A 	Adjective      přídavné jméno
C 	Numeral        číslovka
D 	Adverb         příslovce
I 	Interjection   citoslovce
J 	Conjunction    spojka
N 	Noun           podstatné jméno
P 	Pronoun        zájmeno
V 	Verb           sloveso
R 	Preposition    předložka
T 	Particle       částice
X 	Unknown, Not Determined, Unclassifiable
Z 	Punctuation (also used for the Sentence Boundary token)
'''

ALLOWED_TAGS = frozenset(["A", "D", "N", "V"])

print_now('Loading tagger from {}: '.format(TAGGER))
tagger = Tagger.load(TAGGER)
if not tagger:
    print_now("Cannot load tagger from file '%s'\n" % sys.argv[1])
    sys.exit(1)
print_now('done\n')

forms = Forms()
lemmas = TaggedLemmas()
tokens = TokenRanges()
tokenizer = tagger.newTokenizer()

derivator = tagger.getMorpho().getDerivator()
formatter = DerivationFormatter.newRootDerivationFormatter(derivator)

if tokenizer is None:
    print_now("No tokenizer is defined for the supplied model!")
    sys.exit(1)


# pattern used to strip all non-alphanumeric characters
# use precompiled regex to make it faster
pattern = re.compile('[\W_]+')


# should be called once per subject so the lemmatizer knows the context
#
# i.e. pes (animal) vs peso (currency), compare the output of
#   get_tags("pes", "kočka")
#   get_tags("předmět", "pes")
#
def get_tags(text_block):
    # substitute all non-alnum with spaces
    text_block = pattern.sub(" ", text_block)

    # input the text block into the tokenizer
    tokenizer.setText(text_block)

    # collect the tags into this dictionary
    tags = dict()

    while tokenizer.nextSentence(forms, tokens):
        tagger.tag(forms, lemmas)

        for i in range(len(lemmas)):
            lemma = lemmas[i].lemma
            tag = lemmas[i].tag

            root_lemma = formatter.formatDerivation(lemma)

            token = tokens[i]

            part_of_speech = tag[0]

            # if the token is at least two characters long
            # and if the part of speech of this word is allowed, add the lemma to the bag of tags
            if token.length >= 2 and part_of_speech in ALLOWED_TAGS:
                if root_lemma in tags:
                    tags[root_lemma] += 1
                else:
                    tags[root_lemma] = 1

    return tags
