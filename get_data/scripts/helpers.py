# for sys.stdout.flush()
import sys


def print_now(*args, end="\n"):
    sys.stderr.write(" ".join([format(arg) for arg in args]))
    sys.stderr.write(end)
    sys.stderr.flush()
