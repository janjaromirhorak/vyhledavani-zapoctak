from scripts.helpers import print_now
import csv
import math

import pymysql.cursors

connection = pymysql.connect(host='localhost',
                             user='test',
                             password='test',
                             db='vyhledavani',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

FILES = [
    {
        "name": "index",
        "columns": ["lemma", "subject", "count", "is_title"],
        "table_name": "keyword"
    },
    {
        "name": "subjects",
        "columns": ["code", "title", "annotation", "syllabus", "literature"],
        "table_name": "subjects"
    }
]

try:
    with connection.cursor() as cursor:
        for file_info in FILES:
            name = file_info["name"]
            columns = file_info["columns"]
            table_name = file_info["table_name"]

            input_name = "{}.csv".format(name)

            print_now("Opening file '{}'...".format(input_name))
            with open(input_name, 'r') as input_file:
                num_lines = None
                with open(input_name, 'r') as input_file_line_count:
                    num_lines = sum(1 for line in input_file_line_count)

                csvreader = csv.reader(input_file)

                print_now("Converting CSV into SQL inserts...")

                sql = ("INSERT INTO `{}` ({}) VALUES ({});\n".format(
                    table_name,
                    ",".join(["`{}`".format(column) for column in columns]),
                    ",".join('%s' for column in columns)
                ))

                print_now("Using this query template: {}".format(sql))

                counter = 0
                one_percent = num_lines / 100
                prev = None
                for row in csvreader:
                    percentage = math.floor(counter / one_percent)
                    if percentage != prev:
                        print_now("{} % ({} of {})".format(percentage, counter, num_lines))
                        prev = percentage

                    # print_now(row)
                    # print_now(len(row))

                    cursor.execute(sql, row)
                    counter += 1

                print_now("Commiting SQL changes...")
                connection.commit()
                #     line = ""
                #     first = True
                #     for item in row:
                #         if first:
                #             first = False
                #         else:
                #             line += ","
                #
                #         try:
                #             int(item)
                #             line += format(item)
                #         except ValueError:
                #             line += "'{}'".format(item.replace('\'','\\\''))


finally:
    connection.close()
