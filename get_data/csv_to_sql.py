from scripts.helpers import print_now
import csv
import zipfile

FILES = [
    {
        "name": "index",
        "columns": ["lemma", "subject", "count", "is_title"],
        "table_name": "keyword"
    },
    {
        "name": "subjects",
        "columns": ["code", "title", "annotation", "syllabus", "literature"],
        "table_name": "subjects"
    }
]

for file_info in FILES:
    name = file_info["name"]
    columns = file_info["columns"]
    table_name = file_info["table_name"]

    input_name = "{}.csv".format(name)
    output_name = "{}.sql".format(name)
    output_zip_name = "{}.zip".format(output_name)

    print_now("Opening file '{}' to create '{}'...".format(input_name, output_name))
    with open(input_name, 'r') as input_file, open(output_name, 'w') as output_file:
        csvreader = csv.reader(input_file, delimiter=',', quotechar='\'')

        print_now("Converting CSV into SQL inserts...")
        for row in csvreader:
            line = ""
            first = True
            for item in row:
                if first:
                    first = False
                else:
                    line += ","

                try:
                    int(item)
                    line += format(item)
                except ValueError:
                    line += "'{}'".format(item.replace('\'','\\\''))

            output_file.write("INSERT INTO `{}` ({}) VALUES ({});\n".format(
                table_name,
                ",".join(["`{}`".format(column) for column in columns]),
                line
            ))

    print_now("Creating a zip file '{}' from '{}'...".format(output_zip_name, output_name))
    zip = zipfile.ZipFile(output_zip_name, "w", zipfile.ZIP_DEFLATED)
    zip.write(output_name)
