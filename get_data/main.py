from scripts.get_subject_codes import get_subject_codes
from scripts.get_subject import get_subject
from scripts.helpers import print_now
import numbers

import csv

INDEX_CSV_FILE = "index.csv"
SUBJECTS_CSV_FILE = "subjects.csv"

def create_csv_line(*args):
    return ",".join([
        "{}{}{}".format(
            "" if isinstance(arg, numbers.Number) else "'",
            arg.replace('\'', '\\\'') if isinstance(arg, str) else arg,
            "" if isinstance(arg, numbers.Number) else "'"
        ) for arg in args
    ]) + '\n'

print_now("Retrieving codes for all the subjects...")
# get codes of all the subjects
codes = get_subject_codes()
print_now("done")

#
# codes = [
#     'NMUM501'
# ]

print_now("Creating the main index by joining keywords from individual subjects...")

with open(INDEX_CSV_FILE, 'w') as index_file, open(SUBJECTS_CSV_FILE, 'w') as subjects_file:
        index_csv_writer = csv.writer(index_file)
        subjects_csv_writer = csv.writer(subjects_file)

        first_write_index = True
        first_write_subjects = True

        counter = 0;
        codes_total = len(codes)
        for code in codes:
            counter += 1
            print_now("{} / {}".format(counter, codes_total), end=": ")

            subject_data = get_subject(code)

            code = subject_data["code"]
            title_tags = subject_data["title_tags"]
            text_tags = subject_data["text_tags"]

            subjects_csv_writer.writerow([
                code,
                subject_data["title"],
                subject_data["annotation"],
                subject_data["syllabus"],
                subject_data["literature"]
            ])

            for lemma in title_tags:
                count = title_tags[lemma]
                # index_file.write(create_csv_line(lemma, code, count, 1))
                index_csv_writer.writerow([lemma, code, count, 1])

            for lemma in text_tags:
                count = text_tags[lemma]
                # index_file.write(create_csv_line(lemma, code, count, 0))
                index_csv_writer.writerow([lemma, code, count, 0])

print_now("done")
