<?php
// load database connection config
require_once __DIR__ . '/db_connection.php';

// load NotORM
require_once __DIR__ . '/vendor/notorm/NotORM.php';

// the interface used in graphql.php to resolve queries
interface Resolver {
    public function resolve($root, $args, $context);
}

// class that has connection to the database
class Connected {
    protected $data;

    public function __construct() {
        try {
            $connection = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_LOGIN, DB_PASS);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->data = new NotORM($connection);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
}

// processes the 'search' query
class SearchResult extends Connected implements Resolver
{
    // helper function that recieves an ORM expression and list of lemmas
    // and returns an ORM expression that contains subjects that match all the lemmas
    private function get_subjects_for_lemmas($table, $lemmas) {
        $lemmas_count = count($lemmas);

        $subjects = $table->select("count(lemma) as matched_lemmas, title, code, sum(count) as count, snippet");

        foreach($lemmas as $key => $lemma) {
            if($key===0) {
                $subjects = $subjects->where("lemma", $lemma);
            } else {
                $subjects = $subjects->or("lemma", $lemma);
            }
        }

        return $subjects
                    ->group("code", "matched_lemmas >= $lemmas_count")
                    ->order("code, count DESC,  matched_lemmas DESC");
    }

    // resolve the 'search' query
    public function resolve($root, $args, $context)
    {
        $lemmas = $args['lemma'];

        // ORM expression containing only data for the titles
        $title_table = $this->data->titled_keywords()->where("is_title", 1);

        // ORM expression for all data
        $all_table = $this->data->titled_keywords();

        // get subject for both expressions
        $title_results = $this->get_subjects_for_lemmas($title_table, $lemmas);
        $text_results = $this->get_subjects_for_lemmas($all_table, $lemmas);

        // put the subjects that were matched in the title first,
        // after them put all the other matching subjects

        $discovered = [];
        $results = [];

        foreach ($title_results as $result) {
            $code = $result['code'];
            $discovered[$code] = true;
            $results[] = $result;
        }

        foreach ($text_results as $result) {
            $code = $result['code'];
            if(!array_key_exists($code, $discovered)) {
                $discovered[$code] = true;
                $results[] = $result;
            }
        }

        // return
        return $results;
    }
}

// process the 'subject' query
class Subject extends Connected implements Resolver
{
    public function resolve($root, $args, $context)
    {
        $subject = $this->data->subjects()->where("code", $args["code"])[0];
        return $subject;
    }
}

// mapping of the query names to the resolve functions
return [
    'search' => function($root, $args, $context) {
        $result = new SearchResult();
        return $result->resolve($root, $args, $context);
    },
    'subject' => function($root, $args, $context) {
        $result = new Subject();
        return $result->resolve($root, $args, $context);
    }
];
