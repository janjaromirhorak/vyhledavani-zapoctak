<?php
// built on top of this example: https://github.com/webonyx/graphql-php/tree/master/examples/02-shorthand

// Test this using the following command
// php -S localhost:8080 ./graphql.php
//
// curl http://localhost:8080 -d '{"query": "query { subject(code: \"NAIL078\") {title} }" }'
// curl http://localhost:8080 -d '{"query": "query { search(lemma: [\"web\"]) {title} }" }'
require_once __DIR__ . '/vendor/autoload.php';

use GraphQL\GraphQL;
use GraphQL\Utils\BuildSchema;

try {
    // retrieve the schema
    $schema = BuildSchema::build(file_get_contents(__DIR__ . '/schema.graphqls'));

    // main file that defines the interaciton
    $rootValue = include __DIR__ . '/rootvalue.php';

    // process the input and extract the query
    $rawInput = file_get_contents('php://input');
    $input = json_decode($rawInput, true);
    $query = $input['query'];

    // get variable values, if there are any
    $variableValues = isset($input['variables']) ? $input['variables'] : null;

    // execute the query
    $result = GraphQL::execute($schema, $query, $rootValue, null, $variableValues);
} catch (\Exception $e) {
    $result = [
        'error' => [
            'message' => $e->getMessage()
        ]
    ];
}
// set the header and return the result
header('Content-Type: application/json; charset=UTF-8');
echo json_encode($result);
