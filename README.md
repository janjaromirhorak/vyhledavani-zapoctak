# Vyhledávání předmětů
Jan Horák, III. ročník, ISDI
https://hledani.janjaromirhorak.cz/

## Struktura projektu
- `get_data` -- získání dat ze SISu (Python)
- `client` -- frontend pro webový vyhledávač (JavaScript)
- `server` -- backend pro webový vyhledávač (PHP)
- `web-deploy` -- k instalaci vyhledávače stačí tuto složku překopírovat do kořenového adresáře PHP serveru a importovat do databáze soubor `db.sql.zip`

## 1 Získání dat
Ve složce `get_data` je program napsaný v Pythonu, který ze SISu parsováním HTML získá data, provede jejich lemmatizaci a vytvoří CSV soubory připravené k importu do relační databáze.

Pracoval jsem ve virtuálním prostředí, skript jsem psal pro Python 3.6. Po stažení projektu je nutno nejprve vytvořit virtuální prostředí:
```sh
cd get_data
virtualenv -p /cesta/k/binárnímu/souboru/python3.6 ./
```

a následně nainstalovat závislosti:
```sh
pip3 install -r requirements.txt
```

pak stačí aktivovat virtuální prostředí:
```sh
source bin/activate
```

a po dokončení práce ho opět deaktivovat
```sh
deactivate
```

Hlavním skriptem projektu je `main.py`. Po doběhnutí tohoto skriptu budou v případě úspěchu ve složce `get_data` dva nové soubory obsahující získaná data -- `index.csv` s daty binárního indexu a `subjects.csv` s daty k jednotlivým předmětům (ta jsou použita pro zobrazování výsledků vyhledávání).

### 1.1 Parsování SISu
Seznam předmětů je získán parsováním stránky s výsledky vyhledávání všech předmětů na Matfyzu. Proces je definován v pomocném skriptu `scripts/get_subject_codes.py`.
Skript začíná na první stránce s výsledky a pokračuje stránku po stránce, dokud neobjeví nějakou, která už neobsahuje žádné výsledky, pak končí. K parsování HTML používám knihovnu [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

Tímto způsobem získám všechny kódy předmětů. Pro každý předmět je pak volán pomocný skript `scripts/get_subject.py`, který stáhne stránku předmětu a z HTML extrahuje důležitý text. Na tom pak provede lemmatizaci a pročištění dat a vrátí pole obsahující relevatní informace k předmětu (kód, název, anotaci, syllabus, literaturu a nalezená klíčová slova v titulku a ve zbytku textu). Lemmatizaci a pročištění dat provádí funkce `get_tags` popsaná níže.

### 1.2 Extrakce klíčových slov
K extrakci klíčových slov v textu používám projekt [MorphoDiTa](http://ufal.mff.cuni.cz/morphodita) od ÚFALu. Lemmatizace probíhá ve skriptu `scripts/get_tags.py`. Tagger od MorphoDiTy dostane na vstupu řetězec, na výstupu pak vrátí pole informací ke každému nalezenému slovu. Z těchto informací extrahuji slovní druh, a pokud jde o podstatné či přídavné jméno, sloveso či předložku, je slovo přidáno k tagům vráceným z funkce `get_tags`.

Abych měl podle čeho řadit výsledky, kromě výskytu klíčového slova si ke každému článku získám i počet výskytů klíčového slova, neprovádím však žádnou normalizaci počtů, jde spíše o sekundární pomůcku při řazení (viz níže).

U každého nalezeného klíčového slova též uchovávám informaci, zda bylo nalezeno v titulku či v těle textu. Podle toho jsou pak primárně řazeny výsledky.

### 1.3 Transformace dat k použití v relační databázi
Data jsou pak v `main.py` zapsána do `.csv` souborů, aby mohla být importována do databáze. Pro samotný import jsem napsal skript `csv_to_sql.py`, který naparsuje csv soubory a nahraje je do lokální MySQL databáze.

## 2 Prezentace dat
Získaná data prezentuji ve webovém vyhledávači. Vytvořil jsem frontend a backend, které spolu komunikují prostřednictvím [GraphQL](https://graphql.org/). Frontend je napsaný v JavaScriptu s použitím knihovny React, backend je pak PHP nad MySQL databází.

### 2.1 Backend
Backend poskytuje funkcionalitu vyhledávání nad indexem a přístup k detailům jednotlivých předmětů. Ke komunikaci s databází používám knihovnu [NotORM](https://www.notorm.com/), pro práci s GraphQL pak knihovnu [graphql-php](https://github.com/webonyx/graphql-php).

Pro správu závislostí používám Composer, ale všechny závislosti jsou přibaleny rovnou v Gitu, takže není potřeba nic dalšího provádět. Pro testování backendu na lokálním stroji používám příkaz `php -S localhost:8080 ./graphql.php`.

Nejdůležitější částí backendu je bezesporu vyhledávání, které přijme požadovaná lemmata a vrátí seznam předmětů seřazený podle odhadované relevance. Relevance je odhadována primárně podle toho, zda se klíčová slova vyskytují v názvu předmětů, remízy jsou pak rozhodnuty celkovým počtem výskytů klíčových slov, případné další remízy jsou seřazeny vzestupně podle kódu předmětu.

Ke zjednodušení sestavování výsledku používám MySQL view, který mi k indexu přidá ke každému předmětu ještě titulek a syllabus, používaný jako náhled předmětu. (Vzhledem k současné míře optimalizace MySQL serverů (a možná taky díky množství klíčových slov, které se pohybuje kolem 170 000) se to nepodepsalo na výkonu.)

Pro popis GraphQL schématu slouží soubor `schema.graphqls`, logika aplikace je pak popsána v souboru `rootvalue.php`.

### 2.2 Frontend
Frontend je napsaný v Reactu, základ je tvořen frameworkem [create-react-app](https://github.com/facebook/create-react-app). Jako spouštěč skriptů a správce balíčků používám [yarn](https://yarnpkg.com/). Před prvním spuštěním je potřeba provést instalaci závislostí:
```sh
yarn install
```
Pro testovací prostředí pak slouží `yarn start` a pro sestavení aplikace pro použití v produkčním prostředí `yarn build`.

Hlavní logika aplikace je popsána ve složce `src`. Soubor `App.js` popisuje celé prostředí aplikace a obstarává základní funkci vyhledávače -- při změně obsahu vyhledávacího pole řídí jeho lemmatizaci a zobrazení výsledků.

Lemmatizace a pročištění je popsáno v souboru `getTags.js`. To prostřednictvím REST API MorphoDiTy a identické konfigurace jako u získávání dat získá z vyhledaného řetězce lemmata, která pročistí na pouze požadované slovní druhy.

Formulace dotazu na backend a zpracování výsledků probíhá v komponentě `Subjects`. To pak výsledky zobrazuje prostřednictvím komponenty `SubjectResult`, která má na starosti vzhled jednotlivých výsledků vyhledávání. Po kliknutí na výsledek se zobrazí komponenta `SubjectDetail`, která provede další dotaz na backend, ze kterého získá detaily požadovaného předmětu, které zobrazí.
