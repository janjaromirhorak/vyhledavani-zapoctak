import React from "react";

// Apollo and GraphQL stuff
import { Query } from "react-apollo";
import gql from "graphql-tag";

// Semantic UI stuff
import {
    Header,
    Segment
} from 'semantic-ui-react';

// query for the subject details
const query = gql`
    query Subject($code: String) {
        subject(code: $code) {
            code,
            title,
            annotation,
            syllabus,
            literature
        }
    }
`;

// returns true if the string contains at least one non-whitespace character
function hasContent(string) {
    return string.replace(/^\s+|\s+$/gm,'').length > 0
}

// removes empty fields from the beginning and the end of the array
function trimArray(arr) {
    let firstContent;
    let lastContent = arr.length;

    // find the first and last items that have content
    arr.forEach((item, key) => {
        if(hasContent(item)) {
            if(firstContent === undefined) {
                firstContent = key
            }
            lastContent = key
        }
    })

    // trim the arr
    return arr.slice(firstContent, lastContent + 1)
}

// try to convert plain text to html as best as possible
function plainToHtml(plain) {
    const blocks = trimArray(plain.split("\n"))

    return (
        <div>
            {blocks.map(block => {
                return <p>{block}</p>
            })}
        </div>
    )
}

// query that renders subject details
export default ({ code }) => (
    <Query query={query} variables={{ code }}>
        {({ data, error, loading }) => {
            // loading message
            if (loading) return <Segment basic>načítám…</Segment>;

            // handle errors
            if (error) {
                console.log(error);
                return <Segment basic>Nepodařilo se načíst předmět.</Segment>
            };

            const {subject} = data;

            return (
                // render the subject info
                <div>
                    {/* always show the annotation */}
                    <Segment basic>
                        {plainToHtml(subject.annotation)}
                    </Segment>
                    {/* show the syllabus if it exists */}
                    {subject.syllabus.length > 0
                        ? <Segment basic>
                            <Header as="h4">Syllabus</Header>
                            {plainToHtml(subject.syllabus)}
                        </Segment>
                        : ""
                    }
                    {/* show the literature if it exists */}
                    {subject.literature.length > 0
                        ? <Segment basic>
                            <Header as="h4">Literatura</Header>
                            {plainToHtml(subject.literature)}
                        </Segment>
                        : ""
                    }
                </div>
            );
        }}
    </Query>
);
