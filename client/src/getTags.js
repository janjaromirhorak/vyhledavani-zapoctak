
// url of the MorphoDiTa REST API
const api_url = "https://lindat.mff.cuni.cz/services/morphodita/api/tag"

// the first character of the tag determines the part of speech. we are interested
// in these ones (this has to exactly match the values specified in the data retrieval part of the project)
const allowedTags = new Set(["A", "D", "N", "V"]);

// convert JSON-ready array to a URL string
function toUrlString(arr) {
    let items = []
    for(const item in arr) {
        items.push(`${encodeURIComponent(item)}=${encodeURIComponent(arr[item])}`)
    }
    return items.join("&")
}

// fetch an url asynchronously and handle the errors
async function fetchAsync(url) {
    let response = await fetch(url);
    const contentType = await response.headers.get("content-type");

    let error, data;

    if (contentType && contentType.indexOf("application/json") !== -1) {
        data = await response.json();
    } else {
        error = await response.text();
        console.error("fetchAsync error:", error);
    }

    return {
      error,
      data
    };
}

// return tags for the provided text
export default async (text) => {
    // parameters passed to the MorphoDiTa REST API
    const params = {
        output: "json",
        derivation: "root",
        model: "czech-morfflex-pdt-161115",
        data: text
    }

    // construct the request url
    const request_url = `${api_url}?${toUrlString(params)}`

    // retrieve the data
    const {error, data} = await fetchAsync(request_url);

    // return an error, if there is one
    if(error) {
        return {
            error
        }
    }

    // select the result field from the data
    const {result} = data;

    const lemmas = result[0].filter(item => {
        // filter lemmas so we have only the one allowed ones
        return allowedTags.has(item.tag[0])
    }).map(item => item.lemma); // and extract lemmas from the items

    // return it
    return {
        data: lemmas,
    }
}
