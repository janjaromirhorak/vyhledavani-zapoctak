import React from "react";

// Semantic UI stuff
import {
    List,
    Segment,
    Header,
    Button
} from "semantic-ui-react";

// subject detail fetches further data about the subject and displays it in a component
import SubjectDetail from './SubjectDetail';

class App extends React.Component {
    state = {
        // is the subject detail open?
        open: false
    }

    // toggle open / close of the subject detail
    handleClick = () => {
        this.setState({
            open: !this.state.open
        })
    }

    // stop event propagation, used to stop bubbling when user clicks on the SIS button
    stopPropagation = event => {
        event.stopPropagation();
    }

    render() {
        return (
            <Segment as="a" piled
                key={this.props.code}
                className={"App-searchResult"}
                onClick={this.handleClick}
            >
                <Segment clearing compact basic>
                    <Header as="h3" floated="left">
                        {/* title of this subject and its code */}
                        {this.props.title} — {this.props.code}
                    </Header>
                </Segment>
                {/* depending on the state display a snippet or the whole subject detail */}
                {
                    this.state.open
                    ? <SubjectDetail code={this.props.code}/>
                    : <Segment compact basic>{this.props.snippet}</Segment>
                }
                {/* "view in SIS button" */}
                <Segment clearing compact basic>
                    <Button
                        primary
                        size="small"
                        floated="right"
                        as="a"
                        href={`https://is.cuni.cz/studium/predmety/index.php?do=predmet&kod=${this.props.code}`}
                        target="_blank"
                        onClick={this.stopPropagation}
                    >
                        SIS
                    </Button>
                </Segment>
            </Segment>
        )
    }
}

export default App;
