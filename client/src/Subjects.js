import React from "react";

// Apollo and GraphQL stuff
import { Query } from "react-apollo";
import gql from "graphql-tag";

// subject result handles displaying the subject
import SubjectResult from './SubjectResult'

// Semantic UI stuff
import {
    Header,
    Segment,
    List,
    Dimmer,
    Loader
} from 'semantic-ui-react';

// query for fetching the search results
const query = gql`
    query Search($search: [String]) {
        search(lemma: $search) {
            code,
            title,
            snippet
        }
    }
`;

// a small overkill to format the word "předmět"
function quantity(word, count) {
    const words = {
        "předmět": ["předmět", "předměty", "předmětů"]
    }

    if(words[word]) {
        const variants = words[word];
        if(count >= 5 || count === 0) {
            return variants[2];
        } else if (count >= 2) {
            return variants[1];
        } else if (count === 1) {
            return variants[0];
        }
    }

    return word;
}

// query that handles the search results
export default ({ search }) => {
    return (
        <Query query={query} variables={{ search }}>
            {({ data, error, loading }) => {
                // loading message
                if (loading) {
                    return (
                        <Segment basic>
                            <Segment basic>
                                Hledám předměty...
                            </Segment>
                            <Segment className={"App-searchLoader"}>
                                <Dimmer active inverted>
                                    <Loader inverted />
                                </Dimmer>
                            </Segment>
                        </Segment>
                    )
                }
                // handle the errors
                if (error) {
                    console.log(error);
                    return <Segment basic>Nepodařilo se mi korektně připojit k databázi.</Segment>
                };

                const results = data.search;

                return (
                    <div>
                        <Segment basic>
                            <Segment basic>
                                {/* show the number of results */}
                                Našel jsem <strong>{results.length}</strong>{" "}
                                {quantity("předmět", results.length)}.
                            </Segment>
                            <Segment.Group>
                                {/* pass each result to a separate SubjectResult component */}
                                {results.map((result, key) => {
                                    return (
                                        <SubjectResult key={result.code} title={result.title} code={result.code} snippet={result.snippet}/>
                                    );
                                })}
                            </Segment.Group>
                        </Segment>
                    </div>
                );
            }}
        </Query>
    )
}
