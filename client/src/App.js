import React from 'react';

// Semantic UI stuff
import 'semantic-ui-css/semantic.min.css';
import {
    Container,
    Header,
    Input,
    Segment,
    Icon,
    List
} from 'semantic-ui-react';

// Apollo stuff
import ApolloClient from "apollo-client";
import { ApolloLink } from 'apollo-link';
import { HttpLink } from "apollo-link-http";
import { RetryLink } from "apollo-link-retry";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";

// Subjects (provides Apollo Query for searching subjects)
import Subjects from "./Subjects";

// transforms a string of words into an array of tags using MorphoDiTa's REST API
import getTags from './getTags'

// CSS styles for the app
import './App.css';

// initialize the link
const link = ApolloLink.from([
  new RetryLink(), // retry when the request fails
  new HttpLink({
      // uri of the GraphQL server containing keyword and subject data
      // uri: "/graphql/"
      uri: "http://localhost:8080/"
  })
]);

// initialize the Apollo client
const client = new ApolloClient({
    link: link,
    cache: new InMemoryCache()
});

// the main component of the app
class App extends React.Component {
    // state
    state = {
        search: [],
        loading: false,
        timeout: null
    }

    // called when the search bar content changes
    // waits 200 ms from the last change and than calls handleSearch
    // (to prevent spamming the MorphoDiTa and backend servers with requests when the user is still typing)
    handleChange = (event) => {
        const val = event.target.value;

        // clear the previous timeout
        window.clearTimeout(this.state.timeout)

        if(val.length > 0) {
            // if there is some text in the search bar, start the timeout
            this.setState({
                loading: true,
                timeout: window.setTimeout(() => {
                    this.handleSearch(val)
                }, 200)
            })
        } else {
            // else clear the loading state and search nothing
            this.setState({
                loading: false,
                search: []
            })
        }
    }

    // initiates the search:
    //   1. lemmatizes the search query
    //   2. sets the search state to the lemmas,
    //      so it can be picked up by the Subjects component
    handleSearch = (string) => {
        // get the tags from the search string
        getTags(string).then(({data, error}) => {
            if(!error) {
                // save the recieved data to the search state
                // and clear the loading icon
                this.setState({
                    search: data,
                    loading: false
                })
            } else {
                this.setState({
                    search: [],
                    loading: false
                })
            }
        })
    }

    // render the page content
    render() {
        return (
            // apollo provider so we can use queries on the page
            <ApolloProvider client={client}>
                <div>
                    {/* page header */}
                    <header className="App-header">
                        <Container text textAlign="center">
                            <Header
                                as="h1" inverted className="App-heading"
                                size="huge"
                                style={{
                                    padding: "1em 0"
                                }}
                            >
                                Vyhledávač předmětů
                                <Header.Subheader>
                                    vyučovaných na Matfyzu
                                </Header.Subheader>
                            </Header>
                            {/* search bar */}
                            <Input
                                fluid inverted
                                placeholder="Zadejte klíčová slova"
                                onChange={this.handleChange}
                                loading={this.state.loading}
                                icon='search'
                                id="searchbar"
                            />
                        </Container>
                    </header>
                    {/* main content - search results */}
                    <main>
                        <Container text>
                            {
                                !this.state.search || this.state.search.length === 0 ? (
                                    // if there is nothing in the search state
                                    // display a search icon
                                    <Segment basic textAlign="center" className="App-noSearch">
                                        <Icon name='search' size='huge' />
                                    </Segment>
                                ) : (
                                    // if there is some tags in the search state
                                    // pass it to the Subjects component
                                    // that handles the request to the GraphQL server
                                    <Subjects search={this.state.search} />
                                )
                            }
                        </Container>
                    </main>
                    {/* simple footer to make the page more visually balanced */}
                    <footer className="App-footer">
                        <Container text>
                            <List>
                                <List.Item>
                                    <Icon name='user circle outline' />Jan Horák, III. ročník, ISDI
                                </List.Item>
                                <List.Item>
                                    <Icon name='building' />MFF UK
                                </List.Item>
                            </List>
                        </Container>
                    </footer>
                </div>
            </ApolloProvider>
        );
    }
}

// export the main component
export default App;
